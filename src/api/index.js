// логика работы с локальным хранилищем
export const getCredentuals = () => {
    const credentuals = JSON.parse(localStorage.getItem('credentuals')) || [];
    return credentuals;
};

export const setCredentuals = (obj) => {
    const array = getCredentuals();
    array.push(obj);
    localStorage.setItem('credentuals', JSON.stringify(array));
};

export const getTasks = () => {
    const task = JSON.parse(localStorage.getItem('tasks')) || [];
    return task;
};

export const setTasks = (obj) => {
    const array = getTasks();
    array.push(obj);
    localStorage.setItem('tasks', JSON.stringify(array));
};

export const getActivUser = () => {
    const activUser = JSON.parse(localStorage.getItem('activUser'));
    return activUser;
};

export const setActivUser = (obj) => {
    localStorage.setItem('activUser', JSON.stringify(obj));
};

export const getTaskId = () => {
    const taskId = JSON.parse(localStorage.getItem('taskId')) || 1;
    setTaskId(taskId);
    return taskId;
};

const setTaskId = (id) => {
    id++;
    localStorage.setItem('taskId', JSON.stringify(id));
};

export const getComments = () => {
    const comment = JSON.parse(localStorage.getItem('comments')) || [];
    return comment;
};

export const setComments = (obj) => {
    const array = getComments();
    array.push(obj);
    localStorage.setItem('comments', JSON.stringify(array));
};
